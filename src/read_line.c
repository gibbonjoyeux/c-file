
#include <stdio.h>
#include <unistd.h>
#include "file.h"

////////////////////////////////////////////////////////////////////////////////
/// STATIC FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////
/// FD
//////////////////////////////////////////////////

static t_file*
get_fd(
	t_file			**files,
	int				fd) {
	t_file			*new;
	t_file			*cur;

	cur = *files;
	while (cur && cur->fd != fd)
		cur = cur->next;
	/// EXISTING FILE
	if (cur != NULL)
		return cur;
	/// NEW FILE
	new = malloc(sizeof(t_file));
	if (new == NULL)
		return NULL;
	new->fd = fd;
	new->keep = NULL;
	new->size = 0;
	new->next = *files;
	*files = new;
	return new;
}

static void
remove_fd(
	t_file			**files,
	t_file			*file) {
	t_file			*cur;
	t_file			*prev;

	cur = *files;
	prev = NULL;
	while (cur != NULL) {
		if (cur == file) {
			if (prev != NULL)
				prev->next = cur->next;
			else
				*files = cur->next;
			free(cur);
			return ;
		}
		prev = cur;
		cur = cur->next;
	}
}

//////////////////////////////////////////////////
/// MEMORY
//////////////////////////////////////////////////

static char*
str_sub(
	char			*str,
	size_t			from,
	size_t			length) {
	char			*new_str;
	size_t			i;

	new_str = malloc(length + 1);
	if (new_str == NULL)
		return NULL;
	for (i = 0; i < length; ++i)
		new_str[i] = str[from + i];
	new_str[length] = 0;
	return new_str;
}

//////////////////////////////////////////////////
/// BASICS
//////////////////////////////////////////////////

static uint8_t
check_string(
	t_file			*file,
	char			**line,
	size_t			total_size,
	ssize_t			new_size) {
	size_t			i;

	i = total_size - new_size;
	while (i < total_size) {
		if ((*line)[i] == '\n') {
			file->size = total_size - i - 1;
			if (file->size == 0) {
				file->keep = NULL;
			} else {
				file->keep = str_sub(*line + i + 1, 0, file->size);
				if (file->keep == NULL) {
					free(line);
					return -1;
				}
			}
			(*line)[i] = 0;
			*line = realloc(*line, i + 1);
			return 1;
		}
		i++;
	}
	return 0;
}

static int8_t
read_file(
	t_file			*file,
	char			**line,
	size_t			total_size) {
	ssize_t			new_size;
	char			buff[LINE_BUFF_SIZE + 1];
	int8_t			check;
	ssize_t			i;

	while (1) {
		/// READ FILE TO BUFFER
		new_size = read(file->fd, (char*)buff, LINE_BUFF_SIZE);
		if (new_size == 0) {
			return ((*line != NULL) ? 1 : 0);
		} else if (new_size < 0) {
			free(*line);
			return -1;
		}
		/// RESIZE STRING
		*line = reallocf(*line, total_size + new_size);
		if (*line == NULL)
			return -1;
		/// COPY BUFFER
		for (i = 0; i < new_size; ++i)
			(*line)[total_size + i] = buff[i];
		total_size += (size_t)new_size;
		/// CHECK STRING
		check = check_string(file, line, total_size, new_size);
		if (check != 0)
			return check;
	}
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

int
read_line(
	const int		fd,
	char			**line) {
	static t_file	*files = NULL;
	t_file			*file;
	size_t			total_size;
	ssize_t			new_size;
	int8_t			check;

	if (line == NULL || fd < 0)
		return -1;
	file = get_fd(&files, fd);
	if (file == NULL)
		return -1;
	*line = file->keep;
	total_size = file->size;
	new_size = total_size;
	if ((check = check_string(file, line, total_size, new_size)))
		return check;
	if ((check = read_file(file, line, total_size)) == 1)
		return 1;
	remove_fd(&files, file);
	return check;
}
