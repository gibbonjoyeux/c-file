
#include "file.h"

////////////////////////////////////////////////////////////////////////////////
/// STATIC FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static t_file_lst*
lst_new(
	char		*line) {
	t_file_lst	*lst;

	lst = malloc(sizeof(t_file_lst));
	if (lst == NULL)
		return NULL;
	lst->line = line;
	lst->next = NULL;
	return lst;
}

static void
lst_free(
	t_file_lst	*lst,
	uint8_t		mode) {
	t_file_lst	*next;

	while (lst != NULL) {
		next = lst->next;
		if (mode == 1)
			free(lst->line);
		free(lst);
		lst = next;
	}
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

char**
read_lines(
	const int	fd) {
	int			ret;
	char		**lines;
	char		*line;
	t_file_lst	*lst;
	t_file_lst	*new_node;
	size_t		length;
	size_t		i;

	/// GET LINES
	length = 0;
	lst = NULL;
	while ((ret = read_line(fd, &line))) {
		if (ret < 0) {
			lst_free(lst, 1);
			return NULL;
		}
		new_node = lst_new(line);
		if (new_node == NULL) {
			lst_free(lst, 1);
			return NULL;
		}
		new_node->next = lst;
		lst = new_node;
		length += 1;
	}
	/// FAILURE READING
	if (ret < 0) {
		lst_free(lst, 1);
		return NULL;
	}
	if (lst == NULL)
		return NULL;
	/// ALLOC ARRAY
	lines = calloc(length + 1, sizeof(char*));
	if (lines == NULL)
		return NULL;
	/// FILL ARRAY
	for (i = 0; i < length; ++i) {
		lines[length - i - 1] = lst->line;
		new_node = lst->next;
		free(lst);
		lst = new_node;
	}
	return lines;
}
